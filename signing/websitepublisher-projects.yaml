# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: 2023 Ben Cooksley <bcooksley@kde.org>

# define defaults
defaults:
  # template used to construct the destination that rsync should be syncing to
  # %s will be replaced with the contents of the domain key from the configuration for that repository
  rsyncDestination: autodeploy@tyran.kde.org:/srv/www/generated/%s

documentation/develop-kde-org:
  domain: develop.kde.org
  branches:
    master:

documentation/docs-digikam-org:
  domain: docs.digikam.org
  branches:
    master:

documentation/docs-kdenlive-org:
  domain: docs.kdenlive.org
  branches:
    master:

documentation/docs-krita-org:
  domain: docs.krita.org
  branches:
    krita/5.2:

documentation/openraster-org:
  domain: openraster.org
  branches:
    master:

frameworks/breeze-icons:
  domain: cdn.kde.org/breeze-icons
  branches:
    master:
  # Deploy website icons to the CDN instead, which runs on Tinami
  rsyncDestination: contentdeployer@tinami.kde.org:/srv/www/%s

websites/20years-kde-org:
  domain: 20years.kde.org
  branches:
    master:

websites/25for25-kde-org:
  domain: 25for25.kde.org
  branches:
    master:

websites/25years-kde-org:
  domain: 25years.kde.org
  branches:
    master:

websites/aether-sass:
  domain: cdn.kde.org/aether-devel
  branches:
    master:
  # Deploy website styling resources to the CDN instead, which runs on Tinami
  rsyncDestination: contentdeployer@tinami.kde.org:/srv/www/%s

websites/akademy-kde-org:
  domain: akademy.kde.org
  branches:
    master:
  # Deploy this to Ampel instead for now while we need to keep the Drupal site spliced in
  rsyncDestination: autodeploy@ampel.kde.org:/srv/www/generated/%s

websites/akademy2004-kde-org:
  domain: conference2004.kde.org
  branches:
    master:

websites/akademy2005-kde-org:
  domain: conference2005.kde.org
  branches:
    master:

websites/akademy2006-kde-org:
  domain: akademy2006.kde.org
  branches:
    master:

websites/akademy2007-kde-org:
  domain: akademy2007.kde.org
  branches:
    master:

websites/akademy2008-kde-org:
  domain: akademy2008.kde.org
  branches:
    master:

websites/akademy2009-kde-org:
  domain: akademy2009.kde.org
  branches:
    master:

websites/akademy2010-kde-org:
  domain: akademy2010.kde.org
  branches:
    master:

websites/akademy2012-kde-org:
  domain: akademy2012.kde.org
  branches:
    master:

websites/amarok-kde-org:
  domain: amarok.kde.org
  branches:
    master:

websites/apps-kde-org:
  domain: apps.kde.org
  branches:
    master:

websites/autoconfig-kde-org:
  domain: autoconfig.kde.org
  branches:
    master:

websites/calligra-org:
  domain: calligra.org
  branches:
    master:

websites/cantor-kde-org:
  domain: cantor.kde.org
  branches:
    master:

websites/choqok-kde-org:
  domain: choqok.kde.org
  branches:
    master:

websites/commit-digest-kde-org:
  domain: commit-digest.kde.org
  branches:
    master:

websites/conf-kde-in:
  domain: conf.kde.in
  branches:
    master:

websites/cutehmi-kde-org:
  domain: cutehmi.kde.org
  branches:
    master:

websites/desktopsummit-org:
  domain: desktopsummit.org
  branches:
    master:

websites/digikam-org:
  domain: digikam.org
  branches:
    master:

websites/eco-kde-org:
  domain: eco.kde.org
  branches:
    master:

websites/ev-kde-org:
  domain: ev.kde.org
  branches:
    master:

websites/falkon-org:
  domain: falkon.org
  branches:
    master:

websites/forum-kde-org:
  domain: forum.kde.org
  branches:
    master:

websites/freebsd-kde-org:
  domain: freebsd.kde.org
  branches:
    master:

websites/ghostwriter-kde-org:
  domain: ghostwriter.kde.org
  branches:
    master:

websites/go-kde-org:
  domain: go.kde.org
  branches:
    master:

websites/haruna-kde-org:
  domain: haruna.kde.org
  branches:
    master:

websites/kde-org:
  domain: www.kde.org
  branches:
    master:

websites/jp-kde-org:
  domain: jp.kde.org
  branches:
    master:

websites/juk-kde-org:
  domain: juk.kde.org
  branches:
    master:

websites/kate-editor-org:
  domain: kate-editor.org
  branches:
    master:

websites/kde-china-org:
  domain: kde-china.org
  branches:
    master:

websites/kde-ru:
  domain: kde.ru
  branches:
    master:

websites/kdeconnect-kde-org:
  domain: kdeconnect.kde.org
  branches:
    master:

websites/kdeitalia-it:
  domain: kdeitalia.it
  branches:
    master:

websites/kdemail-net:
  domain: kdemail.net
  branches:
    master:

websites/kdevelop-org:
  domain: kdevelop.org
  branches:
    master:

websites/kexi-project-org:
  domain: kexi-project.org
  branches:
    master:

websites/kgeotag-kde-org:
  domain: kgeotag.kde.org
  branches:
    master:

websites/kid3-kde-org:
  domain: kid3.kde.org
  branches:
    master:

websites/kirogi-org:
  domain: kirogi.org
  branches:
    master:

websites/kmymoney-org:
  domain: kmymoney.org
  branches:
    master:

websites/konsole-kde-org:
  domain: konsole.kde.org
  branches:
    master:

websites/kontact-kde-org:
  domain: kontact.kde.org
  branches:
    master:

websites/konversation-kde-org:
  domain: konversation.kde.org
  branches:
    master:

websites/kpdf-kde-org:
  domain: kpdf.kde.org
  branches:
    master:

websites/kphotoalbum-org:
  domain: kphotoalbum.org
  branches:
    master:

websites/krita-org:
  # Temporary while they build up the new Hugo based replacement site
  domain: dev.krita.org
  branches:
    master:

websites/krusader-org:
  domain: krusader.org
  branches:
    master:

websites/kst-plot-kde-org:
  domain: kst-plot.kde.org
  branches:
    master:

websites/kstars-kde-org:
  domain: kstars.kde.org
  branches:
    master:

websites/lakademy-kde-org:
  domain: lakademy.kde.org
  branches:
    master:

websites/manifesto-kde-org:
  domain: manifesto.kde.org
  branches:
    master:

websites/marble-kde-org:
  domain: marble.kde.org
  branches:
    master:

websites/mentorship-kde-org:
  domain: mentorship.kde.org
  branches:
    master:

websites/minuet-kde-org:
  domain: minuet.kde.org
  branches:
    master:

websites/neon-kde-org:
  domain: neon.kde.org
  branches:
    master:

websites/okular-kde-org:
  domain: okular.kde.org
  branches:
    master:

websites/peruse-kde-org:
  domain: peruse.kde.org
  branches:
    master:

websites/planet-kde-org:
  domain: planet.kde.org
  branches:
    master:

websites/plasma-bigscreen-org:
  domain: plasma-bigscreen.org
  branches:
    master:

websites/plasma-mobile-org:
  domain: plasma-mobile.org
  branches:
    master:

websites/product-screenshots:
  domain: cdn.kde.org/screenshots
  branches:
    master:
  rsyncDestination: contentdeployer@tinami.kde.org:/srv/www/%s

websites/releases-neon-kde-org:
  domain: releases.neon.kde.org
  branches:
    master:

websites/rkward-kde-org:
  domain: rkward.kde.org
  branches:
    master:

websites/rolisteam-org:
  domain: rolisteam.org
  branches:
    master:

websites/scripting-krita-org:
  domain: scripting.krita.org
  branches:
    master:

websites/simon-kde-org:
  domain: simon.kde.org
  branches:
    master:

websites/skrooge-org:
  domain: skrooge.org
  branches:
    master:

websites/subtitlecomposer-kde-org:
  domain: subtitlecomposer.kde.org
  branches:
    master:

websites/timeline-kde-org:
  domain: timeline.kde.org
  branches:
    master:

websites/tr-kde-org:
  domain: tr.kde.org
  branches:
    master:

websites/umbrello-kde-org:
  domain: umbrello.kde.org
  branches:
    master:

websites/vvave-kde-org:
  domain: vvave.kde.org
  branches:
    master:

websites/wiki-desktopsummit-org:
  domain: wiki.desktopsummit.org
  branches:
    master:

websites/wiki-kde-org:
  domain: wiki.kde.org
  branches:
    master:

websites/wiki-rolisteam-org:
  domain: doc.rolisteam.org
  branches:
    master:

websites/zanshin-kde-org:
  domain: zanshin.kde.org
  branches:
    master:
